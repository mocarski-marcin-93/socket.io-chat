var fs = require('fs'),
    http = require('http'),
    socketio = require('socket.io');

var rooms = ['room1', 'room2'];

var server = http.createServer(function(req, res) {
    res.writeHead(200, {
        'Content-type': 'text/html'
    });
    res.end(fs.readFileSync(__dirname + '/index.html'));
}).listen(80, function() {
    console.log('Listening at: http://localhost:80');
});

var io = socketio.listen(server);

io.sockets.on('connection', function(socket) {
    socket.on('room', function(room) {
        rooms.forEach(function(item, index) {
            if (item == room) return;
            socket.broadcast.to(item).emit('message', {
                text: 'Some user has left ' + item,
                toRoom: item,
                byUser: 'CHAT'
            });
            socket.leave(item);
        });
        socket.join(room);
        socket.broadcast.to(room).emit('message', {
            text: 'We got new user in ' + room,
            toRoom: room,
            byUser: 'CHAT'
        });
    });
    socket.on('message', function(msg) {
        io.sockets.to(msg.toRoom).emit('message', msg); // to all include user
        //socket.broadcast.to(msg.toRoom).emit('message', msg); // to all except user
    });
});
